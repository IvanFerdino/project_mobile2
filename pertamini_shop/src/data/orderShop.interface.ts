import { OrderProduct } from "./order_product.interface";

export interface OrderShop{
    delivery_status: string,
    order_status: string,
    product: OrderProduct[],
    sales_order_status: string,
    shop_new_order: string,
    shop_uid: string,
    total_price: string,
    user_lat: string,
    user_lng: string,
    user_uid: string,
    user_name: string,
    shop_name: string,
    order_id: string,
    user_phone: string,
    shop_phone: string,
    user_notes: string
}