export interface shopProduct{
    price_per_volume: string,
    product_name: string,
    status: string,
    stock_volume: string
}