import { Datauser } from "../data/datauser.interface";
import { AuthService } from "./authService";
import { Injectable } from "@angular/core";
import firebase from 'firebase';

@Injectable()
export class CurrentUserDataService {
    public currentUserData: Datauser;
    public dataTemp: any;
    constructor(private authService: AuthService) {
        this.currentUserData = null;
        this.dataTemp = [];
    }
    getAllShop() {
        
    }
    
    reset() {
        this.currentUserData = null;
    }
    addLoggedInUserData(temp: Datauser) {
        this.currentUserData = temp;
    }
    getLoggedInUserData(){
        var uid = this.authService.getCurrentUser().uid;
        console.log("dr service uid", this.authService.getCurrentUser().uid)
        var setData = (temp: any) => {
            console.log('set order data', temp)
            this.currentUserData = temp;
            console.log("inside function",this.currentUserData);
        }

        return new Promise((resolve, reject)=>{
            firebase.database().ref('/DB/' + uid).once('value').then(function(snapshot) {
                setData(snapshot.val());
                resolve( snapshot.val());
            }).catch((err)=>{
                reject(err)
                console.log('err', err)
            });
        })
    }
    updateUserProfile(status, data: any): any{
        let desc="";
        
        console.log("test desv ", status);
        if(status=="profile"){
            desc = this.currentUserData.shop_description;
        }else if(status=="desc"){
            desc = data.shop_description;
        }
        console.log("desc", desc);
        var uid = this.authService.getCurrentUser().uid;
                console.log(data);
                console.log("update", data);
        
                // Get a key for a new Post.
        firebase.database().ref('DB/'+uid).update({name: data.name, phone: data.phone, address: data.address, lat: data.lat, lng: data.lng, shop_description: desc});
        if(this.currentUserData.type=="shop"){
            firebase.database().ref('shop/'+uid).update({name: data.name, phone: data.phone, address: data.address, lat: data.lat, lng: data.lng, shop_description: desc});

        }else{
            firebase.database().ref('user/'+uid).update({name: data.name, phone: data.phone, address: data.address, lat: data.lat, lng: data.lng, shop_description: desc});
        }
        return true;
    }
    updateCurrentUserLocData(data: any) {
        var uid = this.authService.getCurrentUser().uid;
        console.log(data);
        console.log("update uid", data);

        // Get a key for a new Post.
        firebase.database().ref('DB/'+uid).update({lat: data.lat});
        firebase.database().ref('DB/'+uid).update({lng: data.lng});
        if(this.currentUserData.type=="shop"){
            firebase.database().ref('shop/'+uid).update({lat: data.lat});
            firebase.database().ref('shop/'+uid).update({lng: data.lng});

        }else{
            firebase.database().ref('user/'+uid).update({lat: data.lat});
            firebase.database().ref('user/'+uid).update({lng: data.lng});

        }
    }
    refreshCurrentUserData() {
        //tarik lagi dr DB
        var uid;
        if(this.currentUserData == null){
            uid = this.authService.getCurrentUser().uid;
        }else{
            uid = this.currentUserData.UID;
        }
        var setCurrentUserData = (temp: any) => {
            this.currentUserData = temp;
            // console.log("inside function",this.current_userdata);
          }
          firebase.database().ref('/DB/' + uid).once('value').then(function(snapshot) {
            setCurrentUserData(snapshot.val());
          });
          return this.currentUserData;
    }

    getDataShop(){
        console.log("masuk");
        // bisa
        // var setData = (temp: any) => {
        //     console.log("data hasil : "+ temp.email);
        //     this.dataTemp.push(temp);
        //     // console.log("data : "+ this.dataTemp[0].email);
        // }

        // var shopRef = firebase.database().ref('/shop/');
        // shopRef.on('value', function(snapshot){
        //     console.log("cek"+snapshot.val());
        //     let result = snapshot.val();
        //     for(let x in result){
        //         setData(result[x]);
        //         console.log("this "+ result[x].email);
        //     }
        // })
        // bisa

        // return this.dataTemp;
        var setData = (temp: any) => {
            console.log("data hasil : "+ temp.email);
            this.dataTemp = temp;
            // console.log("data : "+ this.dataTemp[0].email);
        }

        return new Promise((resolve, reject)=>{
            var shopRef = firebase.database().ref('/shop/');
            shopRef.once('value').then(function(snapshot){
                let result = snapshot.val();
                resolve(result);
            }).catch((err)=>{
                reject(err)
                console.log('err', err);
            })
        })
    }

}