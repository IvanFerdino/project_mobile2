import firebase from 'firebase';
import { ToastController } from 'ionic-angular';
import { Injectable } from '@angular/core';


@Injectable()
export class AuthService{
    private msgPwd: string;    

    constructor(private toastCtrl: ToastController){}
    
    signup(email: string, password: string) {
        return firebase.auth().createUserWithEmailAndPassword(email, password);
    }

    signin(email: string, password: string) {
        return firebase.auth().signInWithEmailAndPassword(email, password);
    }

    logout() {
        if(firebase.auth().signOut()){
            // this.favList.reset();
            return true;

        }else{
            return false;
        }
    }

    getCurrentUser() {
        return firebase.auth().currentUser;
        //this function rturn object, use ".uid" to get user id of current user
    }

    getCurrentUserToken() {
        return firebase.auth().currentUser.getIdToken();
    }

    updatePassword(newPassword: string, oldPassword: string){
        let user = this.getCurrentUser();        
        let credential = firebase.auth.EmailAuthProvider.credential(
            user.email, 
            oldPassword
        );        

        user.reauthenticateAndRetrieveDataWithCredential(credential).then(() => {
            user.updatePassword(newPassword).then(()=> {
                this.presentToast("Mengubah Kata Sandi ", true);
            }).catch((error) => {
                this.presentToast("Tidak Dapat Mengubah Kata Sandi", false);
            }); 
        }).catch((error) => {
            this.presentToast("Kata Sandi Salah", false);
        });   
    }

    forgotPassword(email: string){
        var auth = firebase.auth();
        var emailAddress = email;

        auth.sendPasswordResetEmail(emailAddress).then(() => {
            this.presentToast("Berhasil Mengirim Email", false);
        }).catch((error) => {
            this.presentToast("Email Tidak Terdaftar", false);
        });
    }

    presentToast(message: any, status: boolean) {
        let toast;
        console.log(message);
        console.log(status);
        if(status){
            toast = this.toastCtrl.create({
                message: 'Berhasil ' + message,
                duration: 3000,
                position: 'bottom'
                });
        }else{
            toast = this.toastCtrl.create({
                message: message,
                duration: 3000,
                position: 'bottom'
                });
        }
        
        
        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });
      
        toast.present();
      }
    
}