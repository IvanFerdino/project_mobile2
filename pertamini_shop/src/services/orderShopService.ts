import { Datauser } from "../data/datauser.interface";
import { AuthService } from "./authService";
import { Injectable } from "@angular/core";
import firebase from 'firebase';
import { OrderShop } from "../data/orderShop.interface";
import { LoadingController } from "ionic-angular";
import { shopProduct } from "../data/shopProduct.interface";

@Injectable()
export class OrderShopService {
    orderData: OrderShop[];
    orderUser: OrderShop[];
    constructor(private authService: AuthService, public loading: LoadingController) {
        // this.getData = this.getData.bind(this)
        this.orderData = []
        this.orderUser = []
    }

    getData (status){
        var uid = this.authService.getCurrentUser().uid;
        console.log("user id ", uid);
        var setOrderData = (temp: any) => {
            console.log('set order data', temp)
            this.orderData = temp;
            console.log("inside function",this.orderData);
        }
        let ref;
        if(status=="user"){
            ref = firebase.database().ref('/order_user/' + uid)
        }else if(status=="shop"){
            ref = firebase.database().ref('/order_shop/' + uid)
        }

        return new Promise((resolve, reject)=>{
            ref.once('value').then(function(snapshot) {
                console.log("size data : ", snapshot.numChildren());
                console.log("cek data : ", snapshot.val());

                setOrderData(snapshot.val());
                resolve( snapshot.val());
            }).catch((err)=>{
                reject(err)
                console.log('err', err)
            });
        })
    }

    updateShopOrder(temp: OrderShop[]) {
            var uid = this.authService.getCurrentUser().uid;
            console.log("update function data1", temp);
    
            // Get a key for a new Post.
            firebase.database().ref('order_shop/'+uid).set(temp);
    }
    updateUserOrder(temp: OrderShop[], user_uid: any) {
        console.log(user_uid)
        console.log("update function data", temp);

        // Get a key for a new Post.
        firebase.database().ref('order_user/'+user_uid).set(temp);
}

    getUserOrderData(temp_uid: any) {
        var setOrderData = (temp: any) => {
            console.log('set order data', temp)
            this.orderUser = temp;
            console.log("inside function",this.orderUser);
        }

        return new Promise((resolve, reject)=>{
            firebase.database().ref('/order_user/' + temp_uid).once('value').then(function(snapshot) {
                console.log("get user data", snapshot.val())
                setOrderData(snapshot.val());
                resolve( snapshot.val());
            }).catch((err)=>{
                reject(err)
                console.log('err', err)
            });
        })
    }

    pushOrder(temp:OrderShop, size:number, idx:number, tempP : shopProduct){
        let sizeUser:Number;
        let sizeShop:Number;
        this.getOrderByUser(temp.user_uid, "user")
        .then((val: any)=>{
            
            firebase.database().ref('order_user/'+temp.user_uid).child(val).set(temp);
            console.log("cek number",Number(val)+1);
        })

        this.getOrderByUser(temp.shop_uid, "shop")
        .then((val: any)=>{
            // sizeShop = val;
            firebase.database().ref('order_shop/'+temp.shop_uid).child(val).set(temp);
        })
        
        this.updateCountOrder(size);
        this.updateStock(idx, tempP, temp.shop_uid);
        console.log("user : "+ sizeUser+" shop : ", sizeShop);
    
        // firebase.database().ref('order_shop/'+temp.shop_uid).push().set(temp);
        // firebase.database().ref('order_user/'+temp.user_uid).push().set(temp);
            
        
    }

    updateCountOrder(size:number){
        console.log("okeoekokeke :"+size);
        console.log("sum :"+(size+1));
        firebase.database().ref('/order_count/').set(size+1);
    }

    updateStock(idx, newData, idShop){
        console.log("data", newData);
        console.log("idx", idx);
        console.log("id", idx);
        firebase.database().ref('/shop_products/').child(idShop).child(idx).set(newData);
    }

    getOrderByUser(id, type){
        var setData = (temp: any) => {
            console.log('cek count'+ temp)
            this.orderUser = temp;
        } 
        let ref:any;
        if(type == "user"){
            ref = firebase.database().ref('order_user/'+id);
        }else if(type == "shop"){
            ref = firebase.database().ref('order_shop/'+id);
        }
        
        return new Promise((resolve, reject)=>{
            ref.once('value').then(function(snapshot) {
                setData(snapshot.numChildren());
                // console.log("size "+snapshot.numChildren());
                resolve(snapshot.numChildren());
            }).catch((err)=>{
                reject(err)
                console.log('err', err)
            });
        })
    }

    getTotalOrder(){
        let resultR:Number;
        var setData = (temp: any) => {
            console.log('cek count'+ temp)
            resultR = temp;
        } 

        return new Promise((resolve, reject)=>{
            firebase.database().ref('/order_count/').once('value').then(function(snapshot) {
                setData(snapshot.val());
                resolve( snapshot.val());
            }).catch((err)=>{
                reject(err)
                console.log('err', err)
            });
        })
    }

    test() {
        return this.orderData;
    }
    // getData(){
    //     var uid = this.authService.getCurrentUser().uid;
    //     return new Promise(function(resolve, reject) {
    //         // Listen for auth state changes.
    //         // [START authstatelistener]
    //         firebase.database().ref('/order_shop/' + uid).once('value').then(function(snapshot) {
    //             resolve(snapshot.val());
    //         }).catch((err)=>{
    //             reject(err);
    //         });
    // //
    //       });
        
    // }

    // getShopOrders() {
    //    var temp = this.getData();
    //    var setData = (x) => {
    //        this.orderData = x;
    //    }
    //    temp.then((result) => {
    //         console.log("abc",temp);
    //         setData(temp);
    //    }).catch((err)=>{
    //         console.log(err);
    //    });
    //     // return this.orderData;
    // }
    // getOrderData() { 
    //     console.log("def",this.orderData);
    //     return this.orderData;
    // }
    // reset() {
    //     this.currentUserData = null;
    // }
    // addLoggedInUserData(temp: Datauser) {
    //     this.currentUserData = temp;
    // }
    // getLoggedInUserData(){
    //     return this.currentUserData;
    // }
    // updateUserProfile(data: any): any{
    //     var uid = this.authService.getCurrentUser().uid;
    //             console.log(data);
    //             console.log("update", data);

    //             // Get a key for a new Post.
    //             firebase.database().ref('DB/'+uid).update({name: data.name, phone: data.phone, address: data.address, lat: data.lat, lng: data.lng});
    //             if(this.currentUserData.type=="shop"){
    //                 firebase.database().ref('shop/'+uid).update({name: data.name, phone: data.phone, address: data.address, lat: data.lat, lng: data.lng});

    //             }else{
    //                 firebase.database().ref('user/'+uid).update({name: data.name, phone: data.phone, address: data.address, lat: data.lat, lng: data.lng});
    //             }
    //             return true;
    // }
    // updateCurrentUserLocData(data: any) {
    //     var uid = this.authService.getCurrentUser().uid;
    //     console.log(data);
    //     console.log("update uid", data);

    //     // Get a key for a new Post.
    //     firebase.database().ref('DB/'+uid).update({lat: data.lat});
    //     firebase.database().ref('DB/'+uid).update({lng: data.lng});
    //     if(this.currentUserData.type=="shop"){
    //         firebase.database().ref('shop/'+uid).update({lat: data.lat});
    //         firebase.database().ref('shop/'+uid).update({lng: data.lng});

    //     }else{
    //         firebase.database().ref('user/'+uid).update({lat: data.lat});
    //         firebase.database().ref('user/'+uid).update({lng: data.lng});

    //     }
    // }
    // refreshCurrentUserData() {
    //     //tarik lagi dr DB
    //     var uid;
    //     if(this.currentUserData == null){
    //         uid = this.authService.getCurrentUser().uid;
    //     }else{
    //         uid = this.currentUserData.UID;
    //     }
    //     var setCurrentUserData = (temp: any) => {
    //         this.currentUserData = temp;
    //         // console.log("inside function",this.current_userdata);
    //       }
    //       firebase.database().ref('/DB/' + uid).once('value').then(function(snapshot) {
    //         setCurrentUserData(snapshot.val());
    //       });
    //       return this.currentUserData;
    // }

}