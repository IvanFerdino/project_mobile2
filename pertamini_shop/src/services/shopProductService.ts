import { Injectable } from "@angular/core";
import firebase from 'firebase';
import { shopProduct } from "../data/shopProduct.interface";
import { AuthService } from "./authService";

@Injectable()
export class ShopProductService {
    shopProduct: shopProduct[];
    constructor(private authService: AuthService) {
        // this.getData = this.getData.bind(this)
        this.shopProduct = []
    }
    getData() {
        // var uid = '';
        // if(uidd == null) {
        //     console.log("uid kosong")
        //     uid = this.authService.getCurrentUser().uid;
        // }else{
        //     console.log("isi")
        //     uid = uidd;
        // }
        // console.log("masuk"+uid);
       var uid = this.authService.getCurrentUser().uid;
        
        var setOrderData = (temp: any) => {
            console.log('set order data', temp)
            this.shopProduct = temp;
            console.log("inside function",this.shopProduct);
        }

        return new Promise((resolve, reject)=>{
            firebase.database().ref('/shop_products/' + uid).once('value').then(function(snapshot) {
                setOrderData(snapshot.val());
                resolve( snapshot.val());
            }).catch((err)=>{
                reject(err)
                console.log('err', err)
            });
        })
    }
    getDataToko(uidd:string) {
        
        
        var setOrderData = (temp: any) => {
            console.log('set order data', temp)
            this.shopProduct = temp;
            console.log("inside function",this.shopProduct);
        }

        return new Promise((resolve, reject)=>{
            firebase.database().ref('/shop_products/' + uidd).once('value').then(function(snapshot) {
                setOrderData(snapshot.val());
                resolve( snapshot.val());
            }).catch((err)=>{
                reject(err)
                console.log('err', err)
            });
        })
    }
    updateShopOrder(temp: shopProduct[]) {
        var uid = this.authService.getCurrentUser().uid;
        console.log("update function data1", temp);

        // Get a key for a new Post.
        firebase.database().ref('shop_products/'+uid).set(temp);

    }
}