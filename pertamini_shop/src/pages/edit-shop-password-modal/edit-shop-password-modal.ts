import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/authService';

/**
 * Generated class for the EditShopPasswordModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-shop-password-modal',
  templateUrl: 'edit-shop-password-modal.html',
})
export class EditShopPasswordModalPage {
  
  passwordForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private authService: AuthService, private toastCtrl: ToastController) {
    this.initializeForm();
  }

  private initializeForm(){
    this.passwordForm = new FormGroup({
      oldPwd: new FormControl(null, Validators.required),
      newPwd: new FormControl(null, [Validators.required, Validators.minLength(8), Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$')]),
      repeatPwd: new FormControl(null, [Validators.required, Validators.minLength(8), Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$')])
    })
  }

  editPassword() {    
    let status = true;

    if(this.passwordForm.value.newPwd != this.passwordForm.value.repeatPwd){
      status = false;

      let toast = this.toastCtrl.create({
        message: 'Pengulangan kata sandi baru salah',
        duration: 3000,
        position: 'bottom'
        });

      toast.present();
      this.passwordForm.reset(); 
    }

    if(status){      
      this.authService.updatePassword(this.passwordForm.value.newPwd, this.passwordForm.value.oldPwd);      
      this.viewCtrl.dismiss();
    }
    
  }

  cancel() {
    this.viewCtrl.dismiss();
  }
}
