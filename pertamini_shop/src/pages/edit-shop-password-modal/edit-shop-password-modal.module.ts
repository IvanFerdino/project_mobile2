import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditShopPasswordModalPage } from './edit-shop-password-modal';

@NgModule({
  declarations: [
    EditShopPasswordModalPage,
  ],
  imports: [
    IonicPageModule.forChild(EditShopPasswordModalPage),
  ],
})
export class EditShopPasswordModalPageModule {}
