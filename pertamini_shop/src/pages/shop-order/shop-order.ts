import { ShopOrderHistoryPage } from './../shop-order-history/shop-order-history';
import { ShopAcceptedOrderPage } from './../shop-accepted-order/shop-accepted-order';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ItemSliding, ModalController, LoadingController } from 'ionic-angular';
import { OrderShopService } from '../../services/orderShopService';
import { OrderShop } from '../../data/orderShop.interface';

import { CallNumber } from '@ionic-native/call-number';

/**
 * Generated class for the ShopOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shop-order',
  templateUrl: 'shop-order.html',
})
export class ShopOrderPage {
  //nama interfacenya salah jadi nya OrderShop, padahal general Order mestinya
  private orderData: OrderShop[];
  private orderUser: OrderShop[];
  private countPendingData: number;
  loading:any;
  // private singleOrderData: OrderShop;
  constructor(public navCtrl: NavController, public navParams: NavParams, private shopOrdersSvc: OrderShopService, private callNumber: CallNumber, private modalCtrl: ModalController,
    public loadingCtr:LoadingController) {
    
      
    
    // console.log(this.shopOrdersSvc.getOrderData());

  }
  call(temp: OrderShop){
    this.callNumber.callNumber(temp.user_phone, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }
  countPendingOrder() {
    if(this.orderData != null){
      let temp = 0;
      // console.log("asda", this.orderData);
      for(let i = 0 ; i< this.orderData.length; i++){
        
        if(this.orderData[i].sales_order_status == 'pending'){
          temp++;
        }
        this.countPendingData = temp;
      }
      console.log("pendingCount", this.countPendingData)
    }else{
      this.countPendingData = 0;
    }
  }

  ionViewDidLoad() {
    this.loading = this.loadingCtr.create({
      content: 'Mohon Menunggu...'
    });
    this.loading.present().then(
      ()=>{
        this.shopOrdersSvc.getData("shop").then( (val: OrderShop[])=>{
          this.orderData = val
          console.log("ghi", this.orderData);
          console.log('ionViewDidLoad ShopOrderPage');
          // this.cekLocalArray();
          console.log("ts", this.orderData);
          this.countPendingOrder();
  
        })
        this.loading.dismiss();
      }
    )
    
  }
  ionViewDidEnter() {
    console.log("did enter")

    this.refresh();

  }
  refresh() {
    this.shopOrdersSvc.getData("shop").then( (val: OrderShop[])=>{
      this.orderData = val
      console.log("ghi", this.orderData);
      console.log('ionViewDidLoad ShopOrderPage');
      // this.cekLocalArray();
      console.log("ts", this.orderData);
    this.countPendingOrder();

    })
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    
        this.shopOrdersSvc.getData("shop").then( (val: OrderShop[])=>{
          this.orderData = val
          console.log("ghi", this.orderData);
          console.log('ionViewDidLoad ShopOrderPage');
          // this.cekLocalArray();
          console.log("ts", this.orderData);
        this.countPendingOrder();
    
        })
        setTimeout(() => {
          console.log('Async operation has ended');
          
          refresher.complete();
          
        });
    
  }

  updateUserOrderData(temp: OrderShop) {
    let user_uid = temp.user_uid;

    //tarik data order si user
    this.shopOrdersSvc.getUserOrderData(user_uid).then( (val: OrderShop[])=>{
      this.orderUser = val
      console.log("userData", this.orderUser);
      console.log('ionViewDidLoad ShopOrderPage');
      // this.cekLocalArray();
      console.log("ts", this.orderUser);

      //update data yg brubah
    for(let i = 0;i<this.orderUser.length;i++) {
      if(this.orderUser[i].order_id == temp.order_id){
        this.orderUser[i] = temp;
      }
    }
    console.log("sesudah update local", this.orderUser);


    //push lagi ke DB
    this.shopOrdersSvc.updateUserOrder(this.orderUser, user_uid);

    })

    
  }

  accept(item: ItemSliding, temp: OrderShop) {
    console.log("sebelum",this.orderData)
    console.log("terima order",temp);
    temp.sales_order_status = "accepted";
    this.countPendingOrder();

    console.log(temp);
    // for(var i = 0 ; i<this.orderData.length; i++) {
    //     if(this.orderData[i].order_id==temp.order_id){
    //       this.orderData[i] = temp;
    //     }
    // }
    item.close();
    console.log("sesudah",this.orderData)
    this.shopOrdersSvc.updateShopOrder(this.orderData);
    this.updateUserOrderData(temp);
    this.ionViewDidLoad();


    //push ke db shop order, push 1 array
    //push ke db user order, tp datanya 1 data order itu sama / temp
    //traik lagi smua dr db
  }

  reject(item: ItemSliding, temp: OrderShop) {
    console.log("sebelum",this.orderData)
    console.log("tolak order",temp);
    temp.sales_order_status = "rejected";
    temp.delivery_status = "rejected";
    temp.order_status = "done";
    this.countPendingOrder();

    console.log(temp);
    // for(var i = 0 ; i<this.orderData.length; i++) {
    //     if(this.orderData[i].order_id==temp.order_id){
    //       this.orderData[i] = temp;
    //     }
    // }
    item.close();
    console.log("sesudah",this.orderData)
    this.shopOrdersSvc.updateShopOrder(this.orderData);
    this.updateUserOrderData(temp);
    this.ionViewDidLoad();
  }

  showBuyerLocation(item: ItemSliding, temp: OrderShop) {
    console.log("lihat lokasi buyer",temp);
    let modal = this.modalCtrl.create("ViewUserLocationModalPage", temp);
    modal.onDidDismiss(data => {
      if(data!=null){console.log(data);}
    });
    modal.present();
    item.close();
  }

  openHistory(){
    this.navCtrl.push(ShopOrderHistoryPage);
  }

}
