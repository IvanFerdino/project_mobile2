import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShopAcceptedOrderPage } from './shop-accepted-order';

@NgModule({
  declarations: [
    ShopAcceptedOrderPage,
  ],
  imports: [
    IonicPageModule.forChild(ShopAcceptedOrderPage),
  ],
})
export class ShopAcceptedOrderPageModule {}
