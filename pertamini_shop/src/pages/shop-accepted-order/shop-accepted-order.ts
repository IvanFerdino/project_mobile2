import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ItemSliding, ModalController, LoadingController } from 'ionic-angular';
import { OrderShop } from '../../data/orderShop.interface';
import { OrderShopService } from '../../services/orderShopService';
import { CallNumber } from '@ionic-native/call-number';

/**
 * Generated class for the ShopAcceptedOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shop-accepted-order',
  templateUrl: 'shop-accepted-order.html',
})
export class ShopAcceptedOrderPage {
//nama interfacenya salah jadi nya OrderShop, padahal general Order mestinya
private orderData: OrderShop[];
private orderUser: OrderShop[];
private countAcceptedData: number;
loading:any;
// private singleOrderData: OrderShop;
  constructor(public navCtrl: NavController, public navParams: NavParams, private shopOrdersSvc: OrderShopService, private callNumber: CallNumber, private modalCtrl: ModalController,
    public loadingCtr: LoadingController) {
      
  }
  call(temp: OrderShop){
    console.log(temp)
    this.callNumber.callNumber(temp.user_phone, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }
  countAcceptedOrder() {
    if(this.orderData != null){
      let temp = 0;
      // console.log("asda", this.orderData);
      for(let i = 0 ; i< this.orderData.length; i++){
        
        if(this.orderData[i].sales_order_status == 'accepted' && this.orderData[i].delivery_status == 'pending'){
          temp++;
        }
        this.countAcceptedData = temp;
      }
      console.log("acceptedCount", this.countAcceptedData)
    }else{
      this.countAcceptedData = 0;
    }
  }

  ionViewDidLoad() {
    this.loading = this.loadingCtr.create({
      content: 'Mohon Menunggu...'
    });
    this.loading.present().then(
      ()=>{
        this.shopOrdersSvc.getData("shop").then( (val: OrderShop[])=>{
          this.orderData = val
          console.log("ghi", this.orderData);
        console.log('ionViewDidLoad ShopAcceptedOrderPage');
          
          // this.cekLocalArray();
          console.log("ts", this.orderData);
        this.countAcceptedOrder();
    
        }) 
        this.loading.dismiss();
      }
    )
      }
  ionViewDidEnter() {
    console.log("did enter")
    
      this.refresh();
  
  }
  refresh() {
    this.shopOrdersSvc.getData("shop").then( (val: OrderShop[])=>{
      this.orderData = val
      console.log("ghi", this.orderData);
      console.log('ionViewDidLoad ShopOrderPage');
      // this.cekLocalArray();
      console.log("ts", this.orderData);
    this.countAcceptedOrder();

    })
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    
        this.shopOrdersSvc.getData("shop").then( (val: OrderShop[])=>{
          this.orderData = val
          console.log("ghi", this.orderData);
          console.log('ionViewDidLoad ShopAcceptedOrderPage');
          // this.cekLocalArray();
          console.log("ts", this.orderData);
        this.countAcceptedOrder();
    
        })
        setTimeout(() => {
          console.log('Async operation has ended');
          
          refresher.complete();
          
        });
     
    
  }

  updateUserOrderData(temp: OrderShop) {
    let user_uid = temp.user_uid;

    //tarik data order si user
    this.shopOrdersSvc.getUserOrderData(user_uid).then( (val: OrderShop[])=>{
      this.orderUser = val
      console.log("userData", this.orderUser);
      console.log('ionViewDidLoad ShopAcceptedOrderPage');
      // this.cekLocalArray();
      console.log("ts", this.orderUser);

      //update data yg brubah
    for(let i = 0;i<this.orderUser.length;i++) {
      if(this.orderUser[i].order_id == temp.order_id){
        this.orderUser[i] = temp;
      }
    }
    console.log("sesudah update local", this.orderUser);


    //push lagi ke DB
    this.shopOrdersSvc.updateUserOrder(this.orderUser, user_uid);

    })

    
  }

  delivered( temp: OrderShop) {
    console.log("sebelum",this.orderData)
    console.log("order terkirim",temp);
    temp.delivery_status = "delivered";
    temp.order_status = "done";
    this.countAcceptedOrder();

    console.log(temp);
    // item.close();
    console.log("sesudah",this.orderData)
    this.shopOrdersSvc.updateShopOrder(this.orderData);
    this.updateUserOrderData(temp);
    this.ionViewDidLoad();
  }

  
  showBuyerLocation(temp: OrderShop) {
    console.log("lihat lokasi buyer",temp);
    let modal = this.modalCtrl.create("ViewUserLocationModalPage", temp);
    modal.onDidDismiss(data => {
      if(data!=null){console.log(data);}
    });
    modal.present();
    // item.close();
  }

}
