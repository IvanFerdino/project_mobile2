import { Component, ViewChild, ElementRef } from '@angular/core';

import { IonicPage, NavController, NavParams, ViewController, Platform, ToastController } from 'ionic-angular';

import { OrderShop } from '../../data/orderShop.interface';
import { Geolocation } from '@ionic-native/geolocation';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
declare var H: any;

/**
 * Generated class for the ViewUserLocationModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-user-location-modal',
  templateUrl: 'view-user-location-modal.html',
})
export class ViewUserLocationModalPage {
   //HERE MAPS
   private platformm: any;
   @ViewChild("mapContainer")
   public mapElement: ElementRef;
   //END
  dataOrder: OrderShop;
  lat: any;
  lng: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private geo: Geolocation, private platform: Platform, private toastCtrl: ToastController,private launchNavigator: LaunchNavigator) {
    this.dataOrder = this.navParams.data;
    console.log("modal liat lokasi user", this.dataOrder.user_lat + "lng" + this.dataOrder.user_lng)
  }

  navigate() {
    //https://ionicframework.com/docs/native/launch-navigator/
    var current_latitude;
    var current_longitude;
    var start_coord;
    var destination_lat =  parseFloat(this.dataOrder.user_lat);
    var destination_lng = parseFloat(this.dataOrder.user_lng);
    var destination = destination_lat+", "+destination_lng;
    this.geo.getCurrentPosition().then(resp => {
      console.log(resp.coords);
      current_latitude = resp.coords.latitude;
      current_longitude = resp.coords.longitude;
      start_coord=current_latitude+", "+current_longitude;

      this.launchNavigator.navigate([destination_lat, destination_lng], {
        start: [current_latitude, current_longitude]
      })
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
      

    })

  }
  cancel() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewUserLocationModalPage');
    
  }
  ionViewDidEnter() {
    this.platformm = new H.service.Platform({
      "app_id": "UJYlNyiXaklkwwLWpdpG",
      "app_code": "c4jKzzaa_Fz4_hEoJts3xg"
    });
    this.platform.ready().then(() => {
      this.geo.getCurrentPosition().then(resp => {
        console.log(resp.coords);
        // this.presentToast(resp.coords.latitude+" "+resp.coords.longitude);
        // this.lat = resp.coords.latitude;
        // this.lng = resp.coords.longitude;

        this.lat = this.dataOrder.user_lat;
        this.lng = this.dataOrder.user_lng;
       
         //HERE MAPS
        let defaultLayers = this.platformm.createDefaultLayers();
        let map = new H.Map(
            this.mapElement.nativeElement,
            defaultLayers.normal.base,
            {
                zoom: 17,
                center: { lat: this.lat, lng: this.lng }
            }
        );

        // Enable the event system on the map instance:
        var mapEvents = new H.mapevents.MapEvents(map);
        // Add event listeners:
        map.addEventListener('tap', function(evt) {
          // Log 'tap' and 'mouse' events:
          console.log(evt.type, evt.currentPointer.type); 
          
          //get current pos coords
          var cur_coords = map.screenToGeo(evt.currentPointer.viewportX, evt.currentPointer.viewportY);
          console.log(cur_coords);

         

        });

        // Instantiate the default behavior, providing the mapEvents object: 
        var behavior = new H.mapevents.Behavior(mapEvents);

        this.showMarker({lat: this.lat, lng: this.lng}, map, behavior);

        //MAP UI
          let ui = H.ui.UI.createDefault(map, defaultLayers);
          // Create an info bubble object at a specific geographic location:
          var bubble = new H.ui.InfoBubble({ lng: this.lng, lat: this.lat }, {
            content: '<b>Lokasi Pembeli!</b>'
          });

          // Add info bubble to the UI:
          ui.addBubble(bubble);
        //END

      }).catch(() => {
        console.log("error to get location!");
        this.lat = 0;        
        this.lng = 0;
      });
    });
  }
  showMarker(resp: any, map: any, behavior: any) {
    var marker = new H.map.Marker({lat: resp.lat, lng: resp.lng});
    marker.draggable = true;
    map.addObject(marker);
 }

}
