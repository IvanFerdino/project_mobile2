import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewUserLocationModalPage } from './view-user-location-modal';

@NgModule({
  declarations: [
    ViewUserLocationModalPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewUserLocationModalPage),
  ],
})
export class ViewUserLocationModalPageModule {}
