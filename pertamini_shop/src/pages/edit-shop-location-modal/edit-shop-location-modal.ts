import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform, ToastController } from 'ionic-angular';


import { Geolocation } from '@ionic-native/geolocation';
import { convertUrlToDehydratedSegments } from 'ionic-angular/umd/navigation/url-serializer';
import { Datauser } from '../../data/datauser.interface';
import { CurrentUserDataService } from '../../services/currentUserDataService';

//HERE MAPS
declare var H: any;
//END

/**
 * Generated class for the EditShopLocationModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-shop-location-modal',
  templateUrl: 'edit-shop-location-modal.html',
})
export class EditShopLocationModalPage {
  //HERE MAPS
  private platformm: any;
  @ViewChild("mapContainer")
  public mapElement: ElementRef;
  //END

  lat: number;
  lng: number;
  data: {lat: any, lng: any};
  alamat: string;
  // datauser: Datauser
  datauser: {address: string,
    email: string,
    lat: any,
    lng: any,
    name: string,
    phone: string,
    shop_description: string,
    type: string,
    UID: string}={address:"",
      email: "",
      lat: "",
      lng: "",
      name: "",
      phone: "",
      shop_description: "",
      type: "",
      UID: ""};
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private geo: Geolocation, private platform: Platform, private toastCtrl: ToastController, private currentUserDataService: CurrentUserDataService) {
    this.refresh();
  }
  ionViewDidEnter(){
    console.log("did enter")
    
  }

  refresh() {
    this.currentUserDataService.getLoggedInUserData().then( (val: Datauser)=>{
      this.datauser = val
      console.log("ghiii", this.datauser);
      console.log('ionViewDidLoad edit shop location modal');
      // this.cekLocalArray();
      console.log("tssssss", this.datauser);
    })
  }
  presentToast(resp: any) {
    let toast = this.toastCtrl.create({
      message: resp,
      duration: 3000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
  ionViewDidLoad() {
    this.refresh();

    console.log('ionViewDidLoad EditShopLocationModalPage');
   //https://developer.here.com/blog/display-here-maps-angular-web-application
    //HERE MAPS
    this.platformm = new H.service.Platform({
      "app_id": "UJYlNyiXaklkwwLWpdpG",
      "app_code": "c4jKzzaa_Fz4_hEoJts3xg"
    });
    //END

    //https://www.youtube.com/watch?v=BoskpGvE7fM
    //GET GEO LOCATION FROM NATIVE GPS
    this.platform.ready().then(() => {
      this.geo.getCurrentPosition().then(resp => {
        console.log(resp.coords);
        // this.presentToast(resp.coords.latitude+" "+resp.coords.longitude);
        // this.lat = resp.coords.latitude;
        // this.lng = resp.coords.longitude;

        this.lat = this.datauser.lat;
        this.lng = this.datauser.lng;
       
         //HERE MAPS
        let defaultLayers = this.platformm.createDefaultLayers();
        let map = new H.Map(
            this.mapElement.nativeElement,
            defaultLayers.normal.base,
            {
                zoom: 17,
                center: { lat: this.lat, lng: this.lng }
            }
        );

        // Enable the event system on the map instance:
        var mapEvents = new H.mapevents.MapEvents(map);
        // Add event listeners:
        map.addEventListener('tap', function(evt) {
          // Log 'tap' and 'mouse' events:
          console.log(evt.type, evt.currentPointer.type); 
          
          //get current pos coords
          var cur_coords = map.screenToGeo(evt.currentPointer.viewportX, evt.currentPointer.viewportY);
          console.log(cur_coords);

         

        });

        // Instantiate the default behavior, providing the mapEvents object: 
        var behavior = new H.mapevents.Behavior(mapEvents);

        this.showMarker({lat: this.lat, lng: this.lng}, map, behavior);

        //MAP UI
          let ui = H.ui.UI.createDefault(map, defaultLayers);
          // Create an info bubble object at a specific geographic location:
          var bubble = new H.ui.InfoBubble({ lng: this.lng, lat: this.lat }, {
            content: '<b>Lokasi Toko Sekarang!</b>'
          });

          // Add info bubble to the UI:
          ui.addBubble(bubble);
        //END

      }).catch(() => {
        console.log("error to get location!");
        this.lat = 0;        
        this.lng = 0;
      });
    });
  }

  showMarker(resp: any, map: any, behavior: any) {
    var marker = new H.map.Marker({lat: resp.lat, lng: resp.lng});
  // Ensure that the marker can receive drag events
  marker.draggable = true;
  map.addObject(marker);

  // disable the default draggability of the underlying map
  // when starting to drag a marker object:
  map.addEventListener('dragstart', function(ev) {
    var target = ev.target;
    if (target instanceof H.map.Marker) {
      behavior.disable();
    }
  }, false);

  var setReturnData = (coord:any) => {
    console.log("asds",coord);
    this.data = { lat: coord.lat, lng: coord.lng}
    // this.data.lat= lat;
    // this.data.lng= lng;
  }
  // re-enable the default draggability of the underlying map
  // when dragging has completed
  map.addEventListener('dragend', function(ev) {
    var target = ev.target;
    if (target instanceof H.map.Marker) {
      behavior.enable();
    }
    var coord = map.screenToGeo(ev.currentPointer.viewportX,
      ev.currentPointer.viewportY);
      console.log(coord);
      setReturnData(coord);
  }, false);

  // Listen to the drag event and move the position of the marker
  // as necessary
   map.addEventListener('drag', function(ev) {
    var target = ev.target,
        pointer = ev.currentPointer;
    if (target instanceof H.map.Marker) {
      target.setPosition(map.screenToGeo(pointer.viewportX, pointer.viewportY));
    }
  }, false);
    
  //ngebuat marker nearby location
    // // Define a variable holding SVG mark-up that defines an icon image:
    // var svgMarkup = '<svg width="24" height="24" ' +
    // 'xmlns="http://www.w3.org/2000/svg">' +
    // '<rect stroke="white" fill="#1b468d" x="1" y="1" width="22" ' +
    // 'height="22" /><text x="12" y="18" font-size="12pt" ' +
    // 'font-family="Arial" font-weight="bold" text-anchor="middle" ' +
    // 'fill="white">↓</text></svg>';

    // // Create an icon, an object holding the latitude and longitude, and a marker:
    // var icon = new H.map.Icon(svgMarkup),
    // coords = { lat: resp.lat, lng: resp.lng },
    // marker = new H.map.Marker(coords, {icon: icon});

    // // Add the marker to the map and center the map at the location of the marker:
    // map.addObject(marker);
    // map.setCenter(coords);
 }

 editShopLoc() {
    this.viewCtrl.dismiss(this.data);
  }
  cancel() {
    this.viewCtrl.dismiss();
  }
}

