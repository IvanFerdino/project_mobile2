import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditShopLocationModalPage } from './edit-shop-location-modal';

@NgModule({
  declarations: [
    EditShopLocationModalPage,
  ],
  imports: [
    IonicPageModule.forChild(EditShopLocationModalPage),
  ],
})
export class EditShopLocationModalPageModule {}
