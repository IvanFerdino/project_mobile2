import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/authService';
/**
 * Generated class for the ForgetPasswordModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forget-password-modal',
  templateUrl: 'forget-password-modal.html',
})
export class ForgetPasswordModalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private toastCtrl: ToastController, private authService: AuthService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgetPasswordModalPage');
  }

  // forgotPwd(form: NgForm){
  //   console.log(form.value);
  // }

  // cancel() {
  //     let data = { 'foo': 'bar' };
  //     console.log("cancel");
  //     this.viewCtrl.dismiss();
  // }
  editPassword(form: NgForm) {        
    
    this.authService.forgotPassword(form.value.email);
    form.reset();
    
  }

  cancel() {
    this.viewCtrl.dismiss();
  }
}
