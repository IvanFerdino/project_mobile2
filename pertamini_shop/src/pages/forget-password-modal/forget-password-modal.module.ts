import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForgetPasswordModalPage } from './forget-password-modal';

@NgModule({
  declarations: [
    ForgetPasswordModalPage,
  ],
  imports: [
    IonicPageModule.forChild(ForgetPasswordModalPage),
  ],
})
export class ForgetPasswordModalPageModule {}
