import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderUserPage } from './order-user';

@NgModule({
  declarations: [
    OrderUserPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderUserPage),
  ],
})
export class OrderUserPageModule {}
