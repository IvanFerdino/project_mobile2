import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetorderModalPage } from './setorder-modal';

@NgModule({
  declarations: [
    SetorderModalPage,
  ],
  imports: [
    IonicPageModule.forChild(SetorderModalPage),
  ],
})
export class SetorderModalPageModule {}
