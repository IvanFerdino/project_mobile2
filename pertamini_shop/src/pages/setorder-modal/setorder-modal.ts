import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ViewController, ToastCmp, ToastController } from 'ionic-angular';
import { Datauser } from '../../data/datauser.interface';
import { ShopProductService } from '../../services/shopProductService';
import { shopProduct } from '../../data/shopProduct.interface';
import { NUMBER_TYPE } from '@angular/compiler/src/output/output_ast';
import { OrderShopService } from '../../services/orderShopService';
import { OrderShop } from '../../data/orderShop.interface';
import { OrderProduct } from '../../data/order_product.interface';

/**
 * Generated class for the SetorderModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-setorder-modal',
  templateUrl: 'setorder-modal.html',
})
export class SetorderModalPage {

  dataShop: Datauser;
  shopProduct: shopProduct[];
  datauser: Datauser;
  showBtn : boolean = false;
  count:number;
  load: any;
  size:number=0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public shopProdServ : ShopProductService,
    public alertCtr : AlertController, public orderShopServ: OrderShopService, public loading:LoadingController,
    public viewCtr: ViewController, public toastCtr: ToastController) {
    
    this.shopProduct = [];
    this.dataShop =  this.navParams.get('data');
    this.datauser = this.navParams.get('user');
    console.log("cek data",this.dataShop);
    console.log("cek user",this.datauser);
    this.orderShopServ.getTotalOrder()
    .then((val: any)=>{
        this.count = val;
        
    })
    // console.log("oekokesada : "+this.dataShop.UID);
  }

  ionViewDidLoad() {
    this.load = this.loading.create({
      content:"Mohon Menunggu..."
    });
    console.log('ionViewDidLoad SetorderModalPage');
    console.log('modal1 : '+this.datauser);
    console.log("modal 2 : ", this.dataShop);
    this.load.present().then(
      ()=>{
        this.shopProdServ.getDataToko(this.dataShop.UID).then( (val: any)=>{
          this.shopProduct = val;
          if(this.shopProduct!=null){
            for(let i of this.shopProduct){
              if(i.status=='show'){
                this.size++;
              }
            }
            
          }
          console.log("okeoek", this.size);
        })
        this.load.dismiss();
      }
    )
    

    
  }

  addBtn(temp,idx){
    console.log("size data : ", this.shopProduct);
    console.log("okeoke"+this.datauser.UID);
    this.load = this.loading.create({
      content:"Memproses..."
    });

    let alert = this.alertCtr.create({
      title: 'Masukkan Detil Pesanan',
      inputs: [
        {
          name: 'qty',
          type: "number",
          placeholder: 'kuantitas/liter'
        },
        {
          name: 'notes',
          placeholder: 'catatan dari pengguna'
        }
      ],
      buttons: [
        {
          text: 'Konfirmasi',
          handler: data => {
            if(data.qty == ""){
              console.log("nothing");
              let toast = this.toastCtr.create({
                message: 'Pesanan Gagal! Masukkan Kuantitas!',
                duration: 3000,
                position: 'bottom'
              });
              toast.present();
              return;
            }else if(data.qty!=""){
              console.log("have data", temp.stock_volume-data.qty);
                let toast = this.toastCtr.create({
                  message: 'Pesanan Sedang Diproses!',
                  duration: 3000,
                  position: 'bottom'
                });
                toast.present();
              console.log("tetsing : "+(this.count));
              let newData : shopProduct = {
                price_per_volume: temp.price_per_volume,
                product_name: temp.product_name,
                status: temp.status,
                stock_volume: (temp.stock_volume-data.qty)+""
              }
              let order:OrderProduct[] = [{
                price_per_volume: temp.price_per_volume,
                product_name: temp.product_name,
                volume: data.qty
              }];
              let tempOrder:OrderShop = {
                delivery_status:"pending", 
                order_status:"ongoing", 
                product: order,
                sales_order_status: "pending",
                shop_new_order: "new",
                  shop_uid: this.dataShop.UID,
                  total_price: (data.qty*temp.price_per_volume)+"",
                  user_lat: this.datauser.lat,
                  user_lng: this.datauser.lng,
                  user_uid: this.datauser.UID,
                  user_name: this.datauser.name,
                  shop_name: this.dataShop.name,
                  order_id: this.count+"",
                  user_phone: this.datauser.phone,
                  shop_phone: this.dataShop.phone,
                  user_notes: data.notes
              }
              this.load.present().then(
                ()=>{
                  this.orderShopServ.pushOrder(tempOrder, this.count, idx, newData);
                  
                  this.viewCtr.dismiss();
                  toast.present();
                  this.load.dismiss();
                }
              )
            }
          
          }
        },
        {
          text: 'Batal',
          role: 'cancel',
          handler: data => {
          }
        }
      ]
    });
    alert.present();
  }

  close(){
    this.viewCtr.dismiss();
  }

}
