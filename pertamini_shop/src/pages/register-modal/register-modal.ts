import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, ModalController, LoadingController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/authService';

import { HttpClient } from '@angular/common/http';
import firebase from 'firebase';

/**
 * Generated class for the RegisterModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-modal',
  templateUrl: 'register-modal.html',
})
export class RegisterModalPage {
  uid: string;
  private formRegistrasi: FormGroup;
  lat: any;
  lng: any;
  address: any;
  loadingSignUp: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private formBuilder: FormBuilder, private toast:ToastController, private modalCtrl: ModalController, private authSvc: AuthService, private loading:LoadingController, private http: HttpClient) {
    this.uid = this.navParams.get('userId');
    console.log(this.uid);
    this.loadingSignUp = this.loading.create({
      content: 'Sedang Mendaftar...'
    });
    this.formRegistrasi = this.formBuilder.group({
      type: [],
      name: [
        "", 
        Validators.compose([
          Validators.required, 
          Validators.minLength(1), 
          Validators.maxLength(10)
        ])
      ],
      email: ["", 
        Validators.compose([Validators.required, Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')])
      ],
      password: ["", 
        Validators.compose([Validators.required, Validators.minLength(8), Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$')])
      ],
      conf_password: ["", 
        Validators.compose([Validators.required, Validators.minLength(8), Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$')])
      ],
      phone:  ["",
       Validators.compose([Validators.required, Validators.pattern['^[0-9]{9, }'], Validators.minLength(9)])
      ],
      address:  ["", Validators.required],
      lat:  ["", Validators.required],
      lng:  ["", Validators.required]
    }, {validator: RegisterModalPage.passwordsMatch});
  }
  static passwordsMatch(cg: FormGroup): {[err: string]: any} {
    let pwd1 = cg.get('password');
    let pwd2 = cg.get('conf_password');
    let rv: {[error: string]: any} = {};
    if ((pwd1.touched || pwd2.touched) && pwd1.value !== pwd2.value) {
      rv['passwordMismatch'] = true;
    }
    return rv;
  }
  pushNewUserToFirebase(type: string, uid: string, data: any): boolean {
    let shop_desc="-";
    if(firebase.database().ref('/'+type+'/' + uid).set({
      type: data.type,
      name: data.name,
      email: data.email,
      phone: data.phone,
      address: data.address,
      lat: data.lat,
      lng: data.lng,
      shop_description: shop_desc,
      UID: uid
    }).then(
      ()=>{
        return true;
      }
    ).catch((err)=>{
      return false;
    })){
      return true;
    }else{
      return true;
    }
  }
  initializeShopProductData(type: string, user_id: string): boolean {
    if(firebase.database().ref('/shop_products/' + user_id).set({
      "0" : {
        "product_name" : "pertalite",
        "price_per_volume": "0",
        "stock_volume": "0",
        "status": "hide"
      },
      "1" : {
        "product_name" : "premium",
        "price_per_volume": "0",
        "stock_volume": "0",
        "status": "hide"
      },
      "2" : {
        "product_name" : "pertamax",
        "price_per_volume": "0",
        "stock_volume": "0",
        "status": "hide"
      },
      "3" : {
        "product_name" : "pertamax_plus",
        "price_per_volume": "0",
        "stock_volume": "0",
        "status": "hide"
      },
      "4" : {
        "product_name" : "solar",
        "price_per_volume": "0",
        "stock_volume": "0",
        "status": "hide"
      },
      "5" : {
        "product_name" : "campur-campur",
        "price_per_volume": "0",
        "stock_volume": "0",
        "status": "hide"
      }
    }).then(
      ()=>{
        return true;
      }
    ).catch((err)=>{
      return false;
    })){
      return true;
    }else{
      return true;
    }
  }
  initializeOrderData(type: string, user_id: string): boolean {
    if(firebase.database().ref('/order_'+type+'/' + user_id).set({
    }).then(
      ()=>{
        return true;
      }
    ).catch((err)=>{
      return false;
    })){
      return true;
    }else{
      return true;
    }
  }

  pushToUserDB(type: string, user_id: string, data: any): boolean {
    let shop_desc="-";
    if(firebase.database().ref('/DB/' + user_id).set({
      type: data.type,
      name: data.name,
      email: data.email,
      phone: data.phone,
      address: data.address,
      lat: data.lat,
      lng: data.lng,
      shop_description: shop_desc,
      UID: user_id
    }).then(
      ()=>{
        return true;
      }
    ).catch((err)=>{
      return false;
    })){
      return true;
    }else{
      return true;
    }
  }
  daftar() {
    console.log(this.formRegistrasi.value);
    this.loadingSignUp.present();
    this.authSvc.signup(this.formRegistrasi.value.email,this.formRegistrasi.value.password).then(
      ()=>{
        console.log("success");
        this.loadingSignUp.dismiss();
        this.authSvc.presentToast("Sign Un", true);
        let uid = this.authSvc.getCurrentUser().uid;
        console.log(this.authSvc.getCurrentUser().uid);
        if(this.pushNewUserToFirebase(this.formRegistrasi.value.type, uid, this.formRegistrasi.value) && this.pushToUserDB(this.formRegistrasi.value.type, uid, this.formRegistrasi.value) && this.initializeShopProductData(this.formRegistrasi.value.type, uid) && this.initializeOrderData(this.formRegistrasi.value.type, uid)) {
          console.log("success");
          this.authSvc.logout();
          this.viewCtrl.dismiss();
        }else{
           console.log("error");
        }
      }
    ).catch((err)=>{
        console.log(err);
        this.loadingSignUp.dismiss();
        this.authSvc.presentToast(err.message, false);
    });
  }
  selectMapsLoc() {
    let profileModal = this.modalCtrl.create("SelectMapsModalPage");
    profileModal.onDidDismiss(data => {
      if(data!=null){
        console.log("return data pilih lokasi:",data);
        this.lat=data.lat;
        this.lng=data.lng;
        this.address = data.address;
      }
    });
    profileModal.present();
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterModalPage');
  }
  cancel() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
}
}
