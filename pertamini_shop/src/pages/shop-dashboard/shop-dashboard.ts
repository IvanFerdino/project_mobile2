import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ItemSliding, AlertController } from 'ionic-angular';
import { CurrentUserDataService } from '../../services/currentUserDataService';
import { Datauser } from '../../data/datauser.interface';
import { shopProduct } from '../../data/shopProduct.interface';
import { ShopProductService } from '../../services/shopProductService';

/**
 * Generated class for the ShopDashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shop-dashboard',
  templateUrl: 'shop-dashboard.html',
})
export class ShopDashboardPage {
  shopProduct: shopProduct[];
  currentUserData:  {address: string,
    email: string,
    lat: any,
    lng: any,
    name: string,
    phone: string,
    shop_description: string,
    type: string,
    UID: string}={address:"",
      email: "",
      lat: "",
      lng: "",
      name: "",
      phone: "",
      shop_description: "",
      type: "",
      UID: ""};
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, private currentUserDataService: CurrentUserDataService, private shopProductService: ShopProductService ) {
    
  }
  checkAndUpdateStatusBasedOnStock() { 
    this.shopProductService.getData().then( (val: shopProduct[])=>{
      this.shopProduct = val
      console.log("shop prod", this.shopProduct);
      console.log('ionViewDidLoad ShopOrderPage');
      // this.cekLocalArray();
      console.log("shop prod", this.shopProduct);
      for(let i = 0 ; i<this.shopProduct.length;i++){
        if(Number(this.shopProduct[i].stock_volume) <= 0){
          this.shopProduct[i].status = "hide";
        }
      }
      console.log("abc", this.shopProduct)
      //update ke db
      this.shopProductService.updateShopOrder(this.shopProduct);
      this.refresh();
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShopDashboardPage');
    this.currentUserDataService.getLoggedInUserData().then( (val: Datauser)=>{
      this.currentUserData = val
      console.log("ghiii", this.currentUserData);
      console.log('ionViewDidLoad ShopOrderPage');
      // this.cekLocalArray();
      console.log("tssssss", this.currentUserData);
    })
    console.log('ionViewDidLoad ShopDashboardPage');

   
    this.shopProductService.getData().then( (val: shopProduct[])=>{
      this.shopProduct = val
      console.log("shop prod", this.shopProduct);
      console.log('ionViewDidLoad ShopOrderPage');
      // this.cekLocalArray();
      console.log("shop prod", this.shopProduct);
    })
    this.checkAndUpdateStatusBasedOnStock()
  }
 doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.currentUserDataService.getLoggedInUserData().then( (val: Datauser)=>{
      this.currentUserData = val
      console.log("ghiii", this.currentUserData);
      console.log('ionViewDidLoad ShopOrderPage');
      // this.cekLocalArray();
      console.log("tssssss", this.currentUserData);
      this.shopProductService.getData().then( (val: shopProduct[])=>{
        this.shopProduct = val
        console.log("shop prod", this.shopProduct);
        console.log('ionViewDidLoad ShopOrderPage');
        // this.cekLocalArray();
        console.log("shop prod", this.shopProduct);
      })
      this.checkAndUpdateStatusBasedOnStock();
    })
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    });
  }
  ionViewDidEnter(){
    console.log("did enter")
    this.refresh();
  }

  refresh() {
    this.currentUserDataService.getLoggedInUserData().then( (val: Datauser)=>{
      this.currentUserData = val
      console.log("ghiii", this.currentUserData);
      console.log('ionViewDidLoad ShopOrderPage');
      // this.cekLocalArray();
      console.log("tssssss", this.currentUserData);
    })
    this.shopProductService.getData().then( (val: shopProduct[])=>{
      this.shopProduct = val
      console.log("shop prod", this.shopProduct);
      console.log('ionViewDidLoad ShopOrderPage');
      // this.cekLocalArray();
      console.log("shop prod", this.shopProduct);
    })
    // this.checkAndUpdateStatusBasedOnStock();
  }

  // ChangeShopDesc() {
  //   console.log("ganti shop desc funct");
  // }

  // edit(item: ItemSliding, x: shopProduct) {
  //   console.log("edit bensin");
  //   // item.close();
  //   this.presentPrompt(item,x);
  // }

  show(item: ItemSliding, x: shopProduct) {
    x.status = 'show'
    console.log(this.shopProduct)
    //update ke DB
    this.shopProductService.updateShopOrder(this.shopProduct);

    //refresh
    this.refresh();
    item.close();
  }

  hide(item: ItemSliding, x: shopProduct) {
    x.status = 'hide'
    console.log(this.shopProduct)
    //update ke DB
    this.shopProductService.updateShopOrder(this.shopProduct);

    //refresh
    this.refresh();

    item.close();
    

  }
  changeShopDesc() {
    let alert = this.alertCtrl.create({
      title: 'Ganti Deskripsi Toko',
      inputs: [
        {
          name: 'shopDesc',
          placeholder: 'deskripsi toko',
          value: this.currentUserData.shop_description
        }
      ],
      buttons: [
        {
          text: 'Perbaharui',
          handler: data => {
            this.currentUserData.shop_description = data.shopDesc
            console.log(this.currentUserData)
            this.currentUserDataService.updateUserProfile("desc", this.currentUserData);
            this.refresh();
          }
        },
        {
          text: 'Batal',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
            // item.close();
          }
        }
      ]
    });
    alert.present();
  }

  price(item: ItemSliding,  x: shopProduct) {
    item.close();//nutup slider di list supaya ga kebuka terus
    let alert = this.alertCtrl.create({
      title: 'Ubah Harga '+x.product_name,
      subTitle: 'Ubah harga per liter',
      inputs: [
        {
          name: 'price',
          placeholder: 'harga per liter',
          type: 'number',
          value: x.price_per_volume
        }
      ],
      buttons: [
        {
          text: 'Perbaharui',
          handler: data => {
            x.price_per_volume = data.price
            console.log(this.shopProduct)
            this.shopProductService.updateShopOrder(this.shopProduct);

            //refresh
            this.refresh();
            //update db, refresh
            // if (User.isValid(data.username, data.password)) {
            //   // logged in!
            // } else {
            //   // invalid login
            //   return false;
            // }
          }
        },
        {
          text: 'Batal',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
            // item.close();
          }
        }
      ]
    });
    alert.present();
  }
  stock(item: ItemSliding,  x: shopProduct) {
    item.close();//nutup slider di list supaya ga kebuka terus
    let alert = this.alertCtrl.create({
      title: 'Ubah Stok '+x.product_name,
      subTitle: 'Ubah stok (liter)',
      inputs: [
        {
          name: 'stock',
          placeholder: 'stok liter',
          type: 'number',
          value: x.stock_volume
        }
      ],
      buttons: [
        {
          text: 'Perbaharui',
          handler: data => {
            x.stock_volume = data.stock
            console.log(this.shopProduct)
            this.shopProductService.updateShopOrder(this.shopProduct);

            //refresh
            this.refresh();
            //update db, refresh
            // if (User.isValid(data.username, data.password)) {
            //   // logged in!
            // } else {
            //   // invalid login
            //   return false;
            // }
          }
        },
        {
          text: 'Batal',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
            // item.close();
          }
        }
      ]
    });
    alert.present();
  }
}
