import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShopDashboardPage } from './shop-dashboard';

@NgModule({
  declarations: [
    ShopDashboardPage,
  ],
  imports: [
    IonicPageModule.forChild(ShopDashboardPage),
  ],
})
export class ShopDashboardPageModule {}
