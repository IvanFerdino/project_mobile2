import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserRootPage } from './user-root';

@NgModule({
  declarations: [
    UserRootPage,
  ],
  imports: [
    IonicPageModule.forChild(UserRootPage),
  ],
})
export class UserRootPageModule {}
