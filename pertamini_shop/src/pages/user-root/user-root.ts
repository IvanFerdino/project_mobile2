import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ListOrderUserPage } from '../list-order-user/list-order-user';
import { OrderUserPage } from '../order-user/order-user';
import { HistoryUserPage } from '../history-user/history-user';
import { SettingUserPage } from '../setting-user/setting-user';


/**
 * Generated class for the UserRootPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-root',
  template: `
  <ion-tabs>
  <ion-tab [root]="orderUserPage" tabTitle="Order" tabIcon="clipboard"></ion-tab>
    <ion-tab [root]="listOrderUserPage" tabTitle="List" tabIcon="list"></ion-tab>
    <ion-tab [root]="historyUserPage" tabTitle="Riwayat" tabIcon="list-box"></ion-tab>
    <ion-tab [root]="settingUserPage" tabTitle="Pengaturan" tabIcon="settings"></ion-tab>
  </ion-tabs>

  `
})
export class UserRootPage {
  listOrderUserPage = ListOrderUserPage;
  orderUserPage = OrderUserPage;
  historyUserPage = HistoryUserPage;
  settingUserPage = SettingUserPage;


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserRootPage'); 
  }

}
