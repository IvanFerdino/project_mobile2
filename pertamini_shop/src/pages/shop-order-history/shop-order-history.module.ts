import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShopOrderHistoryPage } from './shop-order-history';

@NgModule({
  declarations: [
    ShopOrderHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(ShopOrderHistoryPage),
  ],
})
export class ShopOrderHistoryPageModule {}
