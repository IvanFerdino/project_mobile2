import { IonicPage, NavController, NavParams, LoadingController, ItemSliding } from "ionic-angular";
import { Component } from "@angular/core";
import { OrderShop } from "../../data/orderShop.interface";
import { OrderShopService } from "../../services/orderShopService";

/**
 * Generated class for the ShopOrderHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shop-order-history',
  templateUrl: 'shop-order-history.html',
})
export class ShopOrderHistoryPage {
  private orderData: OrderShop[];
  private orderUser: OrderShop[];
  private countDoneData: number;
  loading:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private shopOrdersSvc: OrderShopService,
    public loadingCtr: LoadingController) {
      
  }
  hapus(item: ItemSliding) {
    console.log("histori dihapus");
    item.close();
  }

  countDoneOrder() {
    if(this.orderData != null) {
      let temp = 0;
      // console.log("asda", this.orderData);
      for(let i = 0 ; i< this.orderData.length; i++){
        
        if((this.orderData[i].sales_order_status == 'accepted' && this.orderData[i].delivery_status == 'delivered' && this.orderData[i].order_status == "done") || this.orderData[i].sales_order_status == 'rejected'){
          temp++;
        }
        this.countDoneData = temp;
      }
      console.log("acceptedCount", this.countDoneData)
    }else{
      this.countDoneData = 0;
    }
  }

  ionViewDidLoad() {
    this.loading = this.loadingCtr.create({
      content: 'Mohon Menunggu...'
    });
    this.loading.present().then(
      ()=>{
        this.refresh();
        // this.shopOrdersSvc.getData("shop").then( (val: OrderShop[])=>{
        //   this.orderData = val
        //   console.log("ghi", this.orderData);
        //   console.log('ionViewDidLoad ShopOrderHistoryPage');
        
          
        //   // this.cekLocalArray();
        //   console.log("ts", this.orderData);
        //   this.countDoneOrder();
        // })
        this.loading.dismiss();
      }
    )
    
  }
  ionViewDidEnter() {
    console.log("did enter")
    this.refresh();

  }
  refresh() {
    this.shopOrdersSvc.getData("shop").then( (val: OrderShop[])=>{
      this.orderData = val
      console.log("ghi", this.orderData);
      console.log('ionViewDidLoad ShopOrderHistoryPage');
      // this.cekLocalArray();
      console.log("ts", this.orderData);
    this.countDoneOrder();

    })
  }

  doRefresh(refresher) {
   
        this.shopOrdersSvc.getData("shop").then( (val: OrderShop[])=>{
          this.orderData = val
          console.log("ghi", this.orderData);
          console.log('ionViewDidLoad ShopOrderHistoryPage');
          
          // this.cekLocalArray();
          console.log("ts", this.orderData);
        this.countDoneOrder();
    
        })
        setTimeout(() => {
          console.log('Async operation has ended');
          
          refresher.complete();
        });
   
    console.log('Begin async operation', refresher);
    
  }

  updateUserOrderData(temp: OrderShop) {
    let user_uid = temp.user_uid;

    //tarik data order si user
    this.shopOrdersSvc.getUserOrderData(user_uid).then( (val: OrderShop[])=>{
      this.orderUser = val
      console.log("userData", this.orderUser);
      console.log('ionViewDidLoad ShopOrderHistoryPage');
      // this.cekLocalArray();
      console.log("ts", this.orderUser);

      //update data yg brubah
    for(let i = 0;i<this.orderUser.length;i++) {
      if(this.orderUser[i].order_id == temp.order_id){
        this.orderUser[i] = temp;
      }
    }
    console.log("sesudah update local", this.orderUser);


    //push lagi ke DB
    this.shopOrdersSvc.updateUserOrder(this.orderUser, user_uid);

    })
  }

  
  showBuyerLocation(item: ItemSliding, temp: OrderShop) {
    console.log("lihat lokasi buyer",temp);
    item.close();
  }
}
