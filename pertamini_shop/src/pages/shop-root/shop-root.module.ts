import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShopRootPage } from './shop-root';

@NgModule({
  declarations: [
    ShopRootPage,
  ],
  imports: [
    IonicPageModule.forChild(ShopRootPage),
  ],
})
export class ShopRootPageModule {}
