import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ShopDashboardPage } from '../shop-dashboard/shop-dashboard';
import { ShopOrderPage } from '../shop-order/shop-order';
import { ShopProfilePage } from '../shop-profile/shop-profile';
import { ShopAcceptedOrderPage } from '../shop-accepted-order/shop-accepted-order';
import { ShopOrderHistoryPage } from '../shop-order-history/shop-order-history';
import { CurrentUserDataService } from '../../services/currentUserDataService';

/**
 * Generated class for the ShopRootPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shop-root',
  template: `
  <ion-tabs>
    <ion-tab [root]="shopDashboardPage" tabTitle="Toko" tabIcon="home"></ion-tab>
    <ion-tab [root]="shopOrderPage" tabTitle="Pesanan" tabIcon="paper"></ion-tab>
    <ion-tab [root]="shopAcceptedOrderPage" tabTitle="Berlangsung" tabIcon="cart"></ion-tab>
    
    <ion-tab [root]="shopProfilePage" tabTitle="profil" tabIcon="person"></ion-tab>
  </ion-tabs>
  `
})
export class ShopRootPage {
  shopDashboardPage = ShopDashboardPage;
  shopOrderPage = ShopOrderPage;
  shopAcceptedOrderPage = ShopAcceptedOrderPage;
  shopOrderHistoryPage = ShopOrderHistoryPage;
  shopProfilePage = ShopProfilePage;
  constructor(public navCtrl: NavController, public navParams: NavParams, private currentUserDataService: CurrentUserDataService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShopRootPage');
    // console.log("okeoke");
  }

}
