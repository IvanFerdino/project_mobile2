import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, AlertController, ModalController } from 'ionic-angular';
import { AuthService } from '../../services/authService';
import { CurrentUserDataService } from '../../services/currentUserDataService';
import { Datauser } from '../../data/datauser.interface';

/**
 * Generated class for the SettingUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-setting-user',
  templateUrl: 'setting-user.html',
})
export class SettingUserPage {

  loadingLogout: any;
  datauser: {address: string,
    email: string,
    lat: any,
    lng: any,
    name: string,
    phone: string,
    shop_description: string,
    type: string,
    UID: string}={address:"",
      email: "",
      lat: "",
      lng: "",
      name: "",
      phone: "",
      shop_description: "",
      type: "",
      UID: ""};

  constructor(public navCtrl: NavController, public navParams: NavParams, public authServ : AuthService, public loadingCtr : LoadingController, 
    private currentUserDataService: CurrentUserDataService, public toastCtr: ToastController, 
    public alertCtr : AlertController, public modalCtr:ModalController) {
    this.loadingLogout = this.loadingCtr.create({
      content: 'Sedang Keluar...'
    });

    this.refresh();
  }

  refresh(){
    this.currentUserDataService.getLoggedInUserData().then( (val: Datauser)=>{
      this.datauser = val
      
      console.log("data user : ", this.datauser.name);
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingUserPage');
  }

  logout(){
    
    let alert = this.alertCtr.create({
      title: 'Konfirmasi',
      message: 'Yakin keluar?',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Iya',
          handler: () => {
            this.currentUserDataService.reset();
            this.currentUserDataService.getLoggedInUserData();

            this.loadingLogout.present().then(
              ()=>{
                if(this.authServ.logout()) {
                  this.loadingLogout.dismiss();
                  console.log("SUCCESS!");
                }else{
                  this.loadingLogout.dismiss();
                  let toast = this.toastCtr.create({
                    message: 'Gagal Keluar...',
                    duration: 3000,
                    position: 'bottom'
                  });
                  toast.present();
                }
              }
            )

          }
        }
      ]
    });
    alert.present()
  }

  ChangeProfile() {
    let profileModal = this.modalCtr.create("EditShopProfileModalPage", { userId: 8675309 });
    profileModal.onDidDismiss(data => {
      if(data!=null){
        console.log(data);
      }
      console.log("refresh");
      this.refresh();
      // this.currentUserDataService.refreshCurrentUserData();
    });
    profileModal.present();
  }
  ChangePassword() {
    let profileModal = this.modalCtr.create("EditShopPasswordModalPage", { userId: 8675309 });
    profileModal.present();
  }
  ChangeShopLoc() {
    console.log("maps");
    let profileModal = this.modalCtr.create("EditShopLocationModalPage");
    profileModal.onDidDismiss(data => {
      if(data!=null){
        console.log(data);
        this.currentUserDataService.updateCurrentUserLocData(data);
      }
      console.log("refresh");
      this.refresh();

      // this.currentUserDataService.refreshCurrentUserData();
      // this.current_user = this.currentUserDataService.getLoggedInUserData();
    });
    profileModal.present();
  }
  LogoutFunct() {
    console.log("keluar");
    this.currentUserDataService.reset();
    this.currentUserDataService.getLoggedInUserData();

    // this.navCtrl.setRoot(LoginPage);
    this.loadingLogout.present();
    if(this.authServ.logout()) {
      this.loadingLogout.dismiss();
    }else{
      this.loadingLogout.dismiss();
      let toast = this.toastCtr.create({
        message: 'Gagal Keluar...',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }
  }
}
