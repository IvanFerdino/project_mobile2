import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditShopProfileModalPage } from './edit-shop-profile-modal';

@NgModule({
  declarations: [
    EditShopProfileModalPage,
  ],
  imports: [
    IonicPageModule.forChild(EditShopProfileModalPage),
  ],
})
export class EditShopProfileModalPageModule {}
