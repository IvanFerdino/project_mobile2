import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, ModalController, LoadingController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/authService';

import { HttpClient } from '@angular/common/http';
import firebase from 'firebase';
import { Datauser } from '../../data/datauser.interface';
import { CurrentUserDataService } from '../../services/currentUserDataService';

/**
 * Generated class for the RegisterModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-shop-profile-modal',
  templateUrl: 'edit-shop-profile-modal.html',
})
export class EditShopProfileModalPage {
  private formRegistrasi: FormGroup;
  lat: any;
  lng: any;
  address: any;
  loadingSignUp: any;
  datauser: {address: string,
    email: string,
    lat: any,
    lng: any,
    name: string,
    phone: string,
    shop_description: string,
    type: string,
    UID: string}={address:"",
      email: "",
      lat: "",
      lng: "",
      name: "",
      phone: "",
      shop_description: "",
      type: "",
      UID: ""};
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private formBuilder: FormBuilder, private toast:ToastController, private modalCtrl: ModalController, private authSvc: AuthService, private loading:LoadingController, private http: HttpClient, private currentUserDataService: CurrentUserDataService, private toastCtrl: ToastController) {
    // this.datauser = this.currentUserDataService.getLoggedInUserData();
    this.currentUserDataService.getLoggedInUserData().then( (val: Datauser)=>{
      this.datauser = val
      console.log("ghiii", this.datauser);
      console.log('ionViewDidLoad edit shop profile modal');
      // this.cekLocalArray();
      console.log("tssssss", this.datauser);
    })
    this.loadingSignUp = this.loading.create({
      content: 'Sedang Mendaftar...'
    });
    this.formRegistrasi = this.formBuilder.group({
      name: [
        "", 
        Validators.compose([
          Validators.required, 
          Validators.minLength(1), 
          Validators.maxLength(10)
        ])
      ],
      phone:  ["", Validators.required],
      address:  ["", Validators.required],
      lat:  ["", Validators.required],
      lng:  ["", Validators.required]
    });
  }
  
  update() {
    console.log(this.formRegistrasi.value);
    this.loadingSignUp.present();
    this.currentUserDataService.updateUserProfile("profile", this.formRegistrasi.value);

    this.loadingSignUp.dismiss();

    let toast = this.toastCtrl.create({
        message: 'Berhasil di perbaharui',
        duration: 3000,
        position: 'bottom'
      });

      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });

      toast.present();
      this.viewCtrl.dismiss();

    // this.authSvc.signup(this.formRegistrasi.value.email,this.formRegistrasi.value.password).then(
    //   ()=>{
    //     console.log("success");
    //     this.loadingSignUp.dismiss();
    //     this.authSvc.presentToast("Sign Un", true);
    //     let uid = this.authSvc.getCurrentUser().uid;
    //     console.log(this.authSvc.getCurrentUser().uid);
    //     if(this.pushNewUserToFirebase(this.formRegistrasi.value.type, uid, this.formRegistrasi.value) && this.pushToUserDB(this.formRegistrasi.value.type, uid, this.formRegistrasi.value)) {
    //       console.log("success");
    //       this.authSvc.logout();
    //       this.viewCtrl.dismiss();
    //     }else{
    //        console.log("error");
    //     }
    //   }
    // ).catch((err)=>{
    //     console.log(err);
    //     this.loadingSignUp.dismiss();
    //     this.authSvc.presentToast(err.message, false);
    // });
  }
  selectMapsLoc() {
    let profileModal = this.modalCtrl.create("SelectMapsModalPage");
    profileModal.onDidDismiss(data => {
      if(data!=null){
        console.log("return data pilih lokasi:",data);
        this.lat=data.lat;
        this.lng=data.lng;
        this.address = data.address;
      }
    });
    profileModal.present();
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterModalPage');
  }
  cancel() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
}
}
