import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { CurrentUserDataService } from '../../services/currentUserDataService';
import { OrderShop } from '../../data/orderShop.interface';
import { Datauser } from '../../data/datauser.interface';
import { OrderShopService } from '../../services/orderShopService';

/**
 * Generated class for the ListOrderUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-order-user',
  templateUrl: 'list-order-user.html',
})
export class ListOrderUserPage {

  datauser: {address: string,
    email: string,
    lat: any,
    lng: any,
    name: string,
    phone: string,
    shop_description: string,
    type: string,
    UID: string}={address:"",
      email: "",
      lat: "",
      lng: "",
      name: "",
      phone: "",
      shop_description: "",
      type: "",
      UID: ""};

    dataOrder: OrderShop[];
    size:number=0;
    loading:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public currentUserDataServ : CurrentUserDataService, public orderShopServ: OrderShopService,
    public loadingCtr: LoadingController) {
      this.dataOrder = [];
      this.loading = this.loadingCtr.create({
        content: 'Mohon Menunggu...'
      });
  }

  getDataOrder(){
    this.size = 0;
    this.orderShopServ.getData("user").then( (val: OrderShop[])=>{
      this.dataOrder = val;
      if(this.dataOrder!=null){
        for(let i of this.dataOrder) {
          if(i.order_status!='done') {
            this.size++;
          }
        }
      }
      console.log("cek", this.size);
    })
  }

  refresh() {
    this.currentUserDataServ.getLoggedInUserData().then( (val: Datauser)=>{
      this.datauser = val
      console.log("cek", this.datauser);
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListOrderUserPage');
    this.loading.present().then(
      ()=>{
        this.getDataOrder();
        this.loading.dismiss();
      }
    );

  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    
        this.getDataOrder();
        setTimeout(() => {
          console.log('Async operation has ended');
          this.loading.dismiss();
          refresher.complete();
        });
      }
    
}
