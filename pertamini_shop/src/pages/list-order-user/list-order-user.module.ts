import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListOrderUserPage } from './list-order-user';

@NgModule({
  declarations: [
    ListOrderUserPage,
  ],
  imports: [
    IonicPageModule.forChild(ListOrderUserPage),
  ],
})
export class ListOrderUserPageModule {}
