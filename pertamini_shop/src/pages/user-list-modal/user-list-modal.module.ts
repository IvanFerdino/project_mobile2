import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserListModalPage } from './user-list-modal';

@NgModule({
  declarations: [
    UserListModalPage,
  ],
  imports: [
    IonicPageModule.forChild(UserListModalPage),
  ],
})
export class UserListModalPageModule {}
