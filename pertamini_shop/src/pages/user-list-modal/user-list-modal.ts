import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, LoadingController } from 'ionic-angular';

import { CurrentUserDataService } from '../../services/currentUserDataService';
import { Datauser } from '../../data/datauser.interface';
import { CallNumber } from '@ionic-native/call-number';

/**
 * Generated class for the UserListModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-list-modal',
  templateUrl: 'user-list-modal.html',
})
export class UserListModalPage {
  public dataShop : Datauser[];
  public temp : Datauser[];
  public dataUser:Datauser;
  loading:any;
  
  constructor(public navCtrl: NavController,private modaltr: ModalController, public navParams: NavParams,  private currentUserDataService: CurrentUserDataService, private viewCtrl: ViewController,
    public loadingCtr:LoadingController, private callNumber: CallNumber) {
    
      this.dataShop = this.navParams.get('dataShop');
      this.temp = this.navParams.get('dataShop');
      this.dataUser = this.navParams.get('user');
  }

  ionViewDidLoad() {
   console.log("list toko");
  }

  call(temp){
    console.log("nomer telp",temp)
    this.callNumber.callNumber(temp.phone, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

  getDataShop(){
    // console.log("oekoek");
    this.dataShop = [];
    this.currentUserDataService.getDataShop()
    .then((val: any)=>{
        let result = val;
        
        for(let x in result){
          this.dataShop.push(result[x]);
          // console.log("x : "+result[x]);
          // console.log("data"+result[x].email);
        }
        console.log("data"+this.dataShop[1].UID);
    })
  }

  close(){
    this.viewCtrl.dismiss();
  }

  showModal(temp){
    let newModal = this.modaltr.create("SetorderModalPage", {data: temp, user: this.dataUser});
    newModal.present();

    // console.log("cek hasil : ", temp.email);
  }
  generateInput(){
    this.dataShop = this.temp; 
  }

  onInput(ev : any){
    this.generateInput();
    let serVal = ev.target.value;
    if(serVal && serVal.trim() != ''){
      this.dataShop = this.dataShop.filter((data) => {
        return (data.name.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
      })
    }
    console.log(this.temp);
  }


}
