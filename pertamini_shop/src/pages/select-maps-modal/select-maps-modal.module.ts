import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectMapsModalPage } from './select-maps-modal';

@NgModule({
  declarations: [
    SelectMapsModalPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectMapsModalPage),
  ],
})
export class SelectMapsModalPageModule {}
