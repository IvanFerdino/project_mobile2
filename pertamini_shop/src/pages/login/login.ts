import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, ModalController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/authService';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginResult: {status: boolean, errorData: {code: string, message: string}};
  private signInForm: FormGroup;
  loadingLogin: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, private authService: AuthService, private toastCtrl: ToastController, private loadingCtrl: LoadingController, private modalCtrl: ModalController) {
    this.signInForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    });
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  signUp() {
    // let profileModal = this.modalCtrl.create("RegisterModalPage", { userId: 8675309 });
    // profileModal.onDidDismiss(data => {
    //   if(data!=null){
    //     console.log(data);
    //   }
    // });
    // profileModal.present();

    this.navCtrl.push("RegisterModalPage", {userId: 8675309})
  }
  forgetPassword() {
    let profileModal = this.modalCtrl.create("ForgetPasswordModalPage", { userId: 8675309 });
    profileModal.onDidDismiss(data => {
      if(data!=null){
        console.log(data);
      }
    });
    profileModal.present();
  }

  signin() {
    console.log(this.signInForm.value.email);
    this.loadingLogin = this.loadingCtrl.create({
      content: 'Sedang Masuk...'
    });
    this.loadingLogin.present().then(
      ()=>{
        this.authService.signin(this.signInForm.value.email, this.signInForm.value.password).then(
          ()=>{
            console.log("success");
            this.loadingLogin.dismiss();
            this.authService.presentToast("Masuk", true);
            //retrieve from firebase for this current user, get uid and get token, then insert to local array/service
            
            //yg belom: update pas unfav, pas buka load dr db
          }
        ).catch((err)=>{
            console.log(err);
            this.loadingLogin.dismiss();
            // this.authService.presentToast(err.message, false);
            this.authService.presentToast('Email Pengguna atau Password Salah, Silahkan Coba Lagi', false);
        });
      }
    );
    
  }
  // loginFunct() {
  //   var type: string;
  //   type="shop";
  //   // type="user";
  //   if(type=="user"){
  //     this.navCtrl.setRoot("UserRootPage");
  //   }else{
  //     this.navCtrl.setRoot("ShopRootPage");
  //   }
  // }
}
