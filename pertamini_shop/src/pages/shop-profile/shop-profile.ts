import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, ToastController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { AuthService } from '../../services/authService';
import { CurrentUserDataService } from '../../services/currentUserDataService';
import { Datauser } from '../../data/datauser.interface';

/**
 * Generated class for the ShopProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shop-profile',
  templateUrl: 'shop-profile.html',
})
export class ShopProfilePage {
  loadingLogout: any;
  // current_user: Datauser;
  current_user: {address: string,
    email: string,
    lat: any,
    lng: any,
    name: string,
    phone: string,
    shop_description: string,
    type: string,
    UID: string}={address:"",
      email: "",
      lat: "",
      lng: "",
      name: "",
      phone: "",
      shop_description: "",
      type: "",
      UID: ""};
  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController, private authSvc: AuthService, private loadingCtrl: LoadingController, private toastCtrl: ToastController, private currentUserDataService: CurrentUserDataService) {
   
    this.loadingLogout = this.loadingCtrl.create({
      content: 'Sedang Keluar...'
    });
  }

  refresh() {
    this.currentUserDataService.getLoggedInUserData().then( (val: Datauser)=>{
      this.current_user = val
      console.log("ghiii", this.current_user);
      console.log('ionViewDidLoad shopprofile page');
      // this.cekLocalArray();
      console.log("tssssss", this.current_user);
    })
  }
  ionViewDidEnter() {
    console.log("did enter")
    this.refresh()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShopProfilePage');
    this.currentUserDataService.getLoggedInUserData().then( (val: Datauser)=>{
      this.current_user = val
      console.log("ghiii", this.current_user);
      console.log('ionViewDidLoad shopprofile page');
      // this.cekLocalArray();
      console.log("tssssss", this.current_user);
    })
  }

  ChangeProfile() {
    let profileModal = this.modalCtrl.create("EditShopProfileModalPage", { userId: 8675309 });
    profileModal.onDidDismiss(data => {
      if(data!=null){
        console.log(data);
      }
      console.log("refresh");
      this.refresh();
      // this.currentUserDataService.refreshCurrentUserData();
    });
    profileModal.present();
  }
  ChangePassword() {
    let profileModal = this.modalCtrl.create("EditShopPasswordModalPage", { userId: 8675309 });
    profileModal.present();
  }
  ChangeShopLoc() {
    console.log("maps");
    let profileModal = this.modalCtrl.create("EditShopLocationModalPage");
    profileModal.onDidDismiss(data => {
      if(data!=null){
        console.log(data);
        this.currentUserDataService.updateCurrentUserLocData(data);
      }
      console.log("refresh");
      this.refresh();

      // this.currentUserDataService.refreshCurrentUserData();
      // this.current_user = this.currentUserDataService.getLoggedInUserData();
    });
    profileModal.present();
  }
  LogoutFunct() {
    console.log("keluar");
    this.currentUserDataService.reset();
    this.currentUserDataService.getLoggedInUserData();

    // this.navCtrl.setRoot(LoginPage);
    this.loadingLogout.present();
    if(this.authSvc.logout()) {
      this.loadingLogout.dismiss();
    }else{
      this.loadingLogout.dismiss();
      let toast = this.toastCtrl.create({
        message: 'Gagal Keluar...',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }
  }
}
