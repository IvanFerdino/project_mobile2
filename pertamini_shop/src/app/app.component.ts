import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { ShopRootPage } from '../pages/shop-root/shop-root';
import { UserRootPage } from '../pages/user-root/user-root';

import firebase from 'firebase';
import { AuthService } from '../services/authService';

import { Datauser } from '../data/datauser.interface';
import { CurrentUserDataService } from '../services/currentUserDataService';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  private login: boolean;
  private current_userdata: Datauser;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private authSvc: AuthService, private currentUserService: CurrentUserDataService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    var notificationOpenedCallback = function(jsonData) {
      console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    };

    window["plugins"].OneSignal
      .startInit("3d05055d-7565-41ad-ad6c-d28fdebcefc3", "932755240643")
      .handleNotificationOpened(notificationOpenedCallback)
      .endInit();

    firebase.initializeApp({
      apiKey: "AIzaSyCjm_nLgX6cRymdl1AZ_5mZMW_X1YLtEcw",
      authDomain: "pertamini-86381.firebaseapp.com",
      databaseURL: "https://pertamini-86381.firebaseio.com",
      projectId: "pertamini-86381",
      storageBucket: "pertamini-86381.appspot.com",
      messagingSenderId: "420824687623"
    });
    firebase.auth().onAuthStateChanged(user => {
      if(user){
        //logged in
        // this.onLoad(this.tabsPage);
        var uid = this.authSvc.getCurrentUser().uid;
        var setRootBasedOnType = (temp) => {
          if(temp=="shop"){
            this.rootPage = ShopRootPage;
          }else{
            this.rootPage = UserRootPage;
          }
        }
        // var setCurrentUserData = (temp: any) => {
        //   this.current_userdata = temp;
        //   this.currentUserService.addLoggedInUserData(this.current_userdata);
        //   // console.log("inside function",this.current_userdata);
        // }
        firebase.database().ref('/DB/' + uid).once('value').then(function(snapshot) {
          if(snapshot.val().type == "shop"){
            console.log("snapshott",snapshot.val());
            setRootBasedOnType("shop");
          }else{
            setRootBasedOnType("user");
            // this.authSvc.logout();
          }
        });
        //baca dari db
        this.currentUserService.getLoggedInUserData().then( (val: Datauser)=>{
          this.current_userdata = val
          console.log("ghiii", this.current_userdata);
          console.log('ionViewDidLoad ShopOrderPage');
          // this.cekLocalArray();
          console.log("dr app component", this.current_userdata);
        })
        // firebase.database().ref('/DB/' + uid).on('value', function(snapshot) {
        //   // setCurrentUserData(snapshot.val());
        //   console.log("sukses push ke db")
        //   // console.log("test firebase listener");
        // });
        this.login = true;
        console.log("logged in");
        // this.authSvc.logout();
      }else{
        //not logged in
        // this.onLoad(this.rootPage);
        this.rootPage = LoginPage;
        this.login = false;
        console.log("not logged in");
      }
    });
  }


}

