import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

//ROOT
import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';

//PAGES
import { ShopRootPage } from '../pages/shop-root/shop-root';
import { UserRootPage } from '../pages/user-root/user-root';

//TABS
import { ShopDashboardPage } from '../pages/shop-dashboard/shop-dashboard';
import { ShopOrderPage } from '../pages/shop-order/shop-order';
import { ShopProfilePage } from '../pages/shop-profile/shop-profile';
import { ShopAcceptedOrderPage } from '../pages/shop-accepted-order/shop-accepted-order';


//NATIVE 
import { Geolocation } from '@ionic-native/geolocation';
import { CallNumber } from '@ionic-native/call-number';
import { LaunchNavigator } from '@ionic-native/launch-navigator';

//SERVICE
import { AuthService } from '../services/authService';

//additional
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

//SERVICES
import { CurrentUserDataService } from '../services/currentUserDataService';
import { OrderShopService } from '../services/orderShopService';
import { ShopProductService } from '../services/shopProductService';

//USER
import { OrderUserPage } from '../pages/order-user/order-user';
import { ListOrderUserPage } from '../pages/list-order-user/list-order-user';
import { HistoryUserPage } from '../pages/history-user/history-user';
import { SettingUserPage } from '../pages/setting-user/setting-user';
import { ShopOrderHistoryPage } from '../pages/shop-order-history/shop-order-history';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    ShopDashboardPage,ShopOrderPage,ShopProfilePage,ShopAcceptedOrderPage,ShopOrderHistoryPage,
    ShopRootPage,UserRootPage,
    OrderUserPage, ListOrderUserPage, HistoryUserPage, SettingUserPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    ReactiveFormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    ShopDashboardPage,ShopOrderPage,ShopProfilePage,ShopAcceptedOrderPage,ShopOrderHistoryPage,
    ShopRootPage,UserRootPage,
    OrderUserPage, ListOrderUserPage, HistoryUserPage, SettingUserPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    AuthService,
    CurrentUserDataService,
    OrderShopService,
    CallNumber,
    LaunchNavigator,
    ShopProductService
  ]
})
export class AppModule {}
